<?php
class Cards {
	private $id;
	private $title;
	private $platforms;
	private $description;
	private $price;
	private $files;

	public function getId(){
		return $this->id;
	}
	public function setId($i){
		$this->id = (isset($i)) ?$i :NULL;
	}
	public function getTitle(){
		return $this->title;
	}
	public function setTitle($t){
		$this->title = (isset($t)) ?$t :NULL;
	}
	public function getPlatforms(){
		return $this->platforms;
	}
	public function setPlatforms($pt){
		$this->platforms = (isset($pt)) ?$pt :NULL;
	}
	public function getDescription(){
		return $this->description;
	}
	public function setDescription($d){
		$this->description = (isset($d)) ?$d :NULL;
	}
	public function getPrice(){
		return $this->price;
	}
	public function setPrice($p){
		$this->price = (isset($p)) ?$p :NULL;
	}
	public function getFiles(){
		return $this->files;
	}
	public function setFiles($fl){
		$this->files = (isset($fl)) ?$fl :NULL;
	}


}

?>