<?php

class Historias{
	private $id;
	private $titulo;
	private $descricao;
	private $imagem;
	private $desimagem;
	private $video;
	private $tipov;

	public function getId(){
		return $this->id;
	}
	public function setId($i){
		$this->id = (isset($i)) ?$i :NULL;
	}
	public function getTitulo(){
		return $this->titulo;
	}
	public function setTitulo($ti){
		$this->titulo = (isset($ti)) ?$ti :NULL;
	}
	public function getDescricao(){
		return $this->descricao;
	}
	public function setDescricao($des){
		$this->descricao = (isset($des)) ?$des :NULL;
	}
	public function getImagem(){
		return $this->imagem;
	}
	public function setImagem($imagem){
		$this->imagem = (isset($imagem)) ?$imagem :NULL;
	}
	public function getDesimagem(){
		return $this->desimagem;
	}
	public function setDesimagem($desimagem){
		$this->desimagem = (isset($desimagem)) ?$desimagem :NULL;
	}
	public function getVideo(){
		return $this->video;
	}
	public function setVideo($vi){
		$this->video = (isset($vi)) ?$vi :NULL;
	}
	public function getTipov(){
		return $this->tipov;
	}
	public function setTipov($tv){
		$this->tipov = (isset($tv)) ?$tv :NULL;
	}
	
}

  ?>