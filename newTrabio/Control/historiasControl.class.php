<?php

require_once("Conection.class.php");
require_once("Model/historias.class.php");

class historiasControl{
	public function showHistorias(){
		$conection = new Conection("lib/mysql.ini");
		$comando = $conection->getConection()->prepare("SELECT * FROM historia");
		$comando->execute();
		$res = $comando->fetchAll();
		$list = [];
		foreach($res as $item){
			$historias = new Historias();
			$historias->setId($item->id);
			$historias->setTitulo($item->titulo);
			$historias->setDescricao($item->descricacao);
			$historias->setImagem($item->imagem);
			$historias->setDesimagem($item->descricaoimagem);
			$historias->setVideo($item->video);
			$historias->setTipov($item->tipov);
			array_push($list, $historias);
		}
		$conection-> __destruct();
		return $list;
	}

	public function addHistorias($ad){
		$conection = new Conection("lib/mysql.ini");	
		$titulo = $ad->getTitulo();
		$descricao = $ad->getDescricao();
		$imagem = $ad->getImagem();
		$desimagem = $ad->getDesimagem();
		$video = $ad->getVideo();
		$tipov = $ad->getTipov();
		$sql = "INSERT INTO historia(titulo,descricacao,imagem,descricaoimagem,video,tipov) VALUES(:titulo,:descricao,:imagem,:descricaoimagem,:video,:tipov)";
		$comando = $conection->getConection()->prepare($sql);
		$comando->bindParam(':titulo',$titulo);
		$comando->bindParam(':descricao',$descricao);
		$comando->bindParam(':imagem',$imagem);
		$comando->bindParam(':descricaoimagem',$desimagem);
		$comando->bindParam(':video',$video);
		$comando->bindParam(':tipov',$tipov);
		if($comando->execute()){
			$conection->__destruct();
			return true;
		}else{
			$conection->__destruct();
			return false;
		}
	}

	public function selectId($id){
            $conection = new Conection("lib/mysql.ini");
            $sql = "SELECT * FROM historias WHERE id=:id";
            $comando = $conection->getConection()->prepare($sql);
            $comando->bindValue(":id", $id);
            $comando->execute();
            $res = $comando->fetch();
            $historias = new Cards();
            $historias->setVideo($res->video);
            $historias->setTitulo($res->titulo);
            $historias->setDescrição($res->descrição);
            $historias->setId($res->id);
            $conection->__destruct();
            return $historias;
        }

	public function atualizarhistorias($historias){
		$conection = new Conection("lib/mysql.ini");	
        $comando = $conection->getConection()->prepare("UPDATE historias SET video=:video,titulo=:titulo, descrição=:descrição WHERE id=:id;");
		$comando->bindValue(':video',$historias->getVideo());
		$comando->bindValue(':titulo',$historias->getTitulo());
		$comando->bindValue(':descrição',$historias->getDescrição());
		$comando->bindValue(':id',$historias->getId());
		if($comando->execute()){
			$conection->__destruct();
			return true;
		}else{
			$conection->__destruct();
			return false;
		}
	}

}


  ?>