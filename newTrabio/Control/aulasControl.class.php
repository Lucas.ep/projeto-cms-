<?php

require_once("Conection.class.php");
require_once("Model/Aulas.class.php");

class aulasControl{
	public function showAulas(){
		$conection = new Conection("lib/mysql.ini");
		$comando = $conection->getConection()->prepare("SELECT * FROM aulas");
		$comando->execute();
		$res = $comando->fetchAll();
		$list = [];
		foreach($res as $item){
			$aulas = new aulas();
			$aulas->setId($item->id);
			$aulas->setVideo($item->video);
			$aulas->setTitulo($item->titulo);
			$aulas->setDes($item->des);
			array_push($list, $aulas);
		}
		$conection-> __destruct();
		return $list;
	}

	public function addAulas($ad){
		$conection = new Conection("lib/mysql.ini");	
		$video = $ad->getVideo();
		$titulo = $ad->getTitulo();
		$des = $ad->getDes();
		$sql = "INSERT INTO aulas(video,titulo,des) VALUES(:video,:titulo,:des)";
		$comando = $conection->getConection()->prepare($sql);
		$comando->bindParam(':video',$video);
		$comando->bindParam(':titulo',$titulo);
		$comando->bindParam(':des',$des);
		if($comando->execute()){
			$conection->__destruct();
			return true;
		}else{
			$conection->__destruct();
			return false;
		}
	}
	public function deleteAulas($id){
		$conection = new Conection("lib/mysql.ini");	
		$sql = "DELETE FROM game WHERE id={$id}";
		$comando = $conection->getConection()->prepare($sql);
		$executar = $comando->execute();
		$conection-> __destruct();
	}

	public function selectId($id){
            $conection = new Conection("lib/mysql.ini");
            $sql = "SELECT * FROM aulas WHERE id=:id";
            $comando = $conection->getConection()->prepare($sql);
            $comando->bindValue(":id", $id);
            $comando->execute();
            $res = $comando->fetch();
            $aulas = new Cards();
            $aulas->setVideo($res->video);
            $aulas->setTitulo($res->titulo);
            $aulas->setDescrição($res->descrição);
            $aulas->setId($res->id);
            $conection->__destruct();
            return $aulas;
        }

	public function atualizarAulas($aulas){
		$conection = new Conection("lib/mysql.ini");	
        $comando = $conection->getConection()->prepare("UPDATE aulas SET video=:video,titulo=:titulo, descrição=:descrição WHERE id=:id;");
		$comando->bindValue(':video',$aulas->getVideo());
		$comando->bindValue(':titulo',$aulas->getTitulo());
		$comando->bindValue(':descrição',$aulas->getDescrição());
		$comando->bindValue(':id',$aulas->getId());
		if($comando->execute()){
			$conection->__destruct();
			return true;
		}else{
			$conection->__destruct();
			return false;
		}
	}

}


  ?>