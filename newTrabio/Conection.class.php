<?php
final class Conection{
	//Atributos
	private $conection;
	private $host;
	private $user;
	private $db;
	private $pwd;
	private $type;
	//Encapsulamento
	public function getConection(){
		return $this->conection;
	}
	public function setConection($c){
		$this->conection = (isset($c)) ?$c :NULL;
	}
	public function getHost(){
		return $this->host;
	}
	public function setHost($h){
		$this->host = (isset($h)) ? $h :NULL;
	}
	public function getUser(){
		return $this->user;
	}
	public function setUser($u){
		$this->user = (isset($u)) ?$u :NULL;
	}
	public function getDb(){
		return $this->db;
	}
	public function setDb($d){
		$this->db = (isset($d)) ?$d :NULL;
	}
	public function getPwd(){
		return $this->pwd;
	}
	public function setPwd($p){
		$this->pwd = (isset($p)) ?$p :NULL;
	}
	public function getType(){
		return $this->type;
	}
	public function setType($t){
		$this->type = (isset($t)) ?$t :NULL;
	}

	private function analiseData(){
		if($this->getHost()==NULL || $this->getUser()==NULL || $this->getDb()==NULL || $this->getPwd()==NULL || $this->getType()==NULL){
			return false;
		}else {
			return true;
		}
	}

	//Construtor - Open the conection
	public function __construct($config){
		try {
			if(file_exists("$config")){
				$file = parse_ini_file("$config");
				$this->setHost($file['host']);
				$this->setUser($file['user']);
				$this->setDb($file['name']);
				$this->setType($file['type']);
				$this->setPwd($file['pass']);
				try {
					if($this->analiseData()){
						switch ($this->getType()) {
							case "mysql":
								try{
									$this->setConection(new PDO("mysql:host={$this->getHost()};dbname={$this->getDb()}", $this->getUser(), $this->getPwd()));
								}catch(PDOException $e){
									echo "Error: " . $e->getMessage();
								}
								$this->getConection()->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
								$this->getConection()->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE,PDO::FETCH_OBJ);
							break;
							case "sqllite":
							
							break;
							default:
								throw new Exception("No support!");
						}
					}
				}catch(Exception $e){
					echo "Error: " . $e->getMessage();
				}
			}else{
				throw new Exception("File not found!");
			}
		}catch(Exception $ex){
			echo "Error: " . $ex->getMessage();
		}
	}
	//Destrutor - Destroy the conection
	public function __destruct(){
		$this->setConection(null);
	}
}

?>